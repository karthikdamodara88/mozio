//
//  MozioTestTests.m
//  MozioTestTests
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DiagnoseService.h"

@interface MozioTestTests : XCTestCase

@end

@implementation MozioTestTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}
#pragma mark - tests
- (void)testExample {
    
    NSString * dateString = [[[DiagnoseService alloc] init] getDateString];
    NSString *expectedDateString = @"Jun-04-2016";
    XCTAssertEqualObjects(expectedDateString, dateString);
    
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
