//
//  SingletonTests.m
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Singleton.h"

@interface SingletonTests : XCTestCase

@end

@implementation SingletonTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

#pragma mark - helper methods

- (Singleton *)createUniqueInstance {
    
    return [[Singleton alloc] init];
    
}
- (Singleton *)getSharedInstance {
    
    return [Singleton sharedInstance];
    
}

#pragma mark - tests

- (void)testSingletonSharedInstanceCreated {
    
    XCTAssertNotNil([self getSharedInstance]);
    
}

- (void)testSingletonUniqueInstanceCreated {
    
    XCTAssertNotNil([self createUniqueInstance]);
    
}

- (void)testSingletonReturnsSameSharedInstanceTwice {

    Singleton *singleton = [self getSharedInstance];
    XCTAssertEqual(singleton, [self getSharedInstance]);
    
}

- (void)testSingletonSharedInstanceSeparateFromUniqueInstance {
    
    Singleton *singleton = [self getSharedInstance];
    XCTAssertNotEqual(singleton, [self createUniqueInstance]);
}

- (void)testSingletonReturnsSeparateUniqueInstances {
    
    Singleton *singleton = [self createUniqueInstance];
    XCTAssertNotEqual(singleton, [self createUniqueInstance]);
}

- (void)testPatientEntity {
    
    Singleton *singleton = [Singleton sharedInstance];
    
    NSString *className = NSStringFromClass([[singleton getPatientEntity] class]);
    NSString *expectedClassName = @"NSManagedObject";
    XCTAssertEqualObjects(expectedClassName, className);
    
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
