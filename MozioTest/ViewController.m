//
//  ViewController.m
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import "ViewController.h"
#import "PatientDetailsViewController.h"
#import "PatientsTableViewController.h"

#define kTextFieldHeight 30.0f
#define kHeightConstant 40.0f

@interface ViewController ()<UITextFieldDelegate, DiagnoseDelegate>{
    CGFloat selectedHeightFromDown;
}



@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.navigationItem.title = @"Patient Details";
    [self initializeUI];
}

-(void)viewDidAppear:(BOOL)animated{
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillShowNotification];
    [[NSNotificationCenter defaultCenter] removeObserver:UIKeyboardWillHideNotification];
    [self.view endEditing:YES];
}

-(void)initializeUI{
    
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar setBarStyle:UIBarStyleBlackTranslucent];
    [toolbar sizeToFit];
    UIBarButtonItem *buttonflexible = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                       target:nil action:nil];
    UIBarButtonItem *buttonDone = [[UIBarButtonItem alloc]
                                        initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneClicked:)];
    [toolbar setItems:[NSArray arrayWithObjects:buttonflexible,buttonDone, nil]];
    
   NSArray *ageGroupList = @[@"Children (0-15)",@"Young Adult(16-35)"
                        ,@"Mature Adult (36-55)",@"Senior Citizen (more than 55)"];
    _ageGroupField.itemList = ageGroupList;
    _ageGroupField .inputAccessoryView = toolbar;
    _ageGroupField .rightViewMode = UITextFieldViewModeAlways;
    _ageGroupField .rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downarrow.png"]];
    
    NSArray *genderList = @[@"Male", @"Female"];
    _genderField.itemList = genderList;
    _genderField.inputAccessoryView = toolbar;
    _genderField.rightViewMode = UITextFieldViewModeAlways;
    _genderField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downarrow.png"]];
    
    NSArray *usingHallucinogenicDrugs = @[@"YES", @"NO"];
    _medicineField.itemList = usingHallucinogenicDrugs;
    _medicineField.inputAccessoryView = toolbar;
    _medicineField.rightViewMode = UITextFieldViewModeAlways;
    _medicineField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downarrow.png"]];
    
    NSArray *symptomsList = @[@"Migraine", @"Fever", @"Cold", @"Cough"];
    _symptomsField.itemList = symptomsList;
    _symptomsField.inputAccessoryView = toolbar;
    _symptomsField.rightViewMode = UITextFieldViewModeAlways;
    _symptomsField.rightView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"downarrow.png"]];
    
     [self addRightBarButtonItem];
    
}

-(void)addRightBarButtonItem{
    UIBarButtonItem *listButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"List"
                                                                        style:UIBarButtonItemStylePlain target:self action:@selector(listButtonAction)];
    self.navigationItem.rightBarButtonItem = listButtonItem;
}

-(void)listButtonAction{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PatientsTableViewController *patientTableviewVC = [storyboard instantiateViewControllerWithIdentifier:@"PatientsTableViewController"];
    [self.navigationController pushViewController:patientTableviewVC animated:YES];
}


-(void)keyboardWillShow:(NSNotification *)notificatio{
    
        NSDictionary* userInfo = [notificatio userInfo];
        // get the size of the keyboard
        CGSize keyboardSize = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    if(keyboardSize.height > selectedHeightFromDown){
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y -= ((keyboardSize.height - selectedHeightFromDown) + kHeightConstant);
    
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [self.view setFrame:viewFrame];
        [UIView commitAnimations];
    }
    else{
        [self setToNormalView];
    }
    
    
}

-(void)keyboardWillHide:(NSNotification *)notification{
 
    [self setToNormalView];
}

-(void)setToNormalView{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [self.view setFrame:[UIScreen mainScreen].bounds];
    [UIView commitAnimations];
}


#pragma mark - UITextField delegate methods

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    selectedHeightFromDown = (self.view.frame.size.height - (textField.frame.origin.y + kTextFieldHeight));
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

-(void)doneClicked:(id)sender{
    [self.view endEditing:YES];
    
}
- (IBAction)diagnoseButonClicked:(id)sender {
    [self.view endEditing:YES];
    if([self isValidValues]){
            DiagnoseService *service = [DiagnoseService alloc];
            service.delegate = self;
            [service diagnosePatientWithName:_patientNameField.text withAgeGroup:_ageGroupField.selectedItem
                                withGender: _genderField.selectedItem withMedicine:_medicineField.selectedItem
                                            withSymptoms:_symptomsField.selectedItem];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"All values are required" message:@"" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }

}

-(BOOL)isValidValues{
    
     if([_patientNameField.text isEqualToString:@""] || !(_ageGroupField.selectedItem)
       || !(_genderField.selectedItem)
       ||!(_medicineField.selectedItem)
       || !(_symptomsField.selectedItem)){
        
         return false;
    }
    return true;
 
}
    
-(void)setFieldsToDefaultValues{
    _patientNameField.text = @"";
    [_ageGroupField setSelectedItem:@"" animated:YES];
    [_genderField setSelectedItem:@""];
    [_symptomsField setSelectedItem:@""];
    [_medicineField setSelectedItem:@""];
}

#pragma mark - Diagnose Delegate Methods
-(void)didSucceedRequest:(Patient *)patient{
    
    [self setFieldsToDefaultValues];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PatientDetailsViewController *patientDetailsVC = [storyboard instantiateViewControllerWithIdentifier:@"PatientDetailsViewController"];
    patientDetailsVC.patientdetails = patient;
    [self.navigationController pushViewController:patientDetailsVC animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
