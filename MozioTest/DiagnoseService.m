//
//  DiagnoseService.m
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import "DiagnoseService.h"
#import "Singleton.h"


@interface DiagnoseService(){
    int count;
}

@end
static NSString* const SYMPTOM = @"Migraine";
static NSString* const AGE = @"Children (0-15)";
static NSString* const GENDER = @"Male";
static NSString* const HallucinogenicMEDICE = @"YES";


@implementation DiagnoseService
-(void)diagnosePatientWithName:(NSString *)name withAgeGroup:(NSString*)ageGroup withGender:
                                                            (NSString *)gender withMedicine:
                                                            (NSString *)medicineUsage
                                                              withSymptoms:(NSString *)symptoms;{
    
    // check the conditions
    if([ageGroup isEqualToString:AGE]){
        count++;
    }
    if([symptoms isEqualToString:SYMPTOM]){
        count++;
    }
    if([medicineUsage isEqualToString:HallucinogenicMEDICE]){
        count++;
    }
    if([gender isEqualToString:GENDER]){
        count++;
    }
    
    NSString *result = @"";
    
    if(count == 4){
      result = @"Positive";
        
    }else if(count == 3){
       result = @"75% Probability for T odd’s Syndrome";
    }
    else if(count == 2){
       result = @"50% Probability for T odd’s Syndrome";
    }
    else{
     result = @"Negative";
    }
    
    Patient *patient = [[Singleton sharedInstance] getPatientEntity];
    patient.patientName = name;
    patient.ageGroup = ageGroup;
    patient.gender = gender;
    patient.medicineUsage = medicineUsage;
    patient.symptoms = symptoms;
    patient.result = result;
    patient.date = [self getDateString];
   
    
    // save patient to database
    [[Singleton sharedInstance] saveEntity];
    
    // send the results to the delegate
    if([self.delegate respondsToSelector:@selector(didSucceedRequest:)]){
            [self.delegate didSucceedRequest:patient];
        }
    
}
    // Get fommated date
-(NSString *)getDateString{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMM-dd-yyyy"];
    NSDate *now = [[NSDate alloc] init];
    return [format stringFromDate:now];
}

@end
