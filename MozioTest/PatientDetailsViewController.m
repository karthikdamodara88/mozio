//
//  PatientDetailsViewController.m
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import "PatientDetailsViewController.h"

@interface PatientDetailsViewController ()

@end

@implementation PatientDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self showPatientDetails];
}

-(void)showPatientDetails{
    _patientNameLbl.text = _patientdetails.patientName;
    _ageGroupLbl.text    = _patientdetails.ageGroup;
    _symptomsLbl.text    = _patientdetails.symptoms;
    _genderLbl.text      = _patientdetails.gender;
    _dateLbl.text        = _patientdetails.date;
    _drugUsageLabel.text = _patientdetails.medicineUsage;
    _resultLbl.text      = _patientdetails.result;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
