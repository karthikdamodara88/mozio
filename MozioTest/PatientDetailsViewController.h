//
//  PatientDetailsViewController.h
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Patient.h"

@interface PatientDetailsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *patientNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *ageGroupLbl;
@property (weak, nonatomic) IBOutlet UILabel *genderLbl;
@property (weak, nonatomic) IBOutlet UILabel *drugUsageLabel;
@property (weak, nonatomic) IBOutlet UILabel *symptomsLbl;
@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UITextView *resultLbl;

@property(strong, nonatomic)Patient* patientdetails;
@end
