//
//  DiagnoseService.h
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Patient.h"

@protocol DiagnoseDelegate <NSObject>
-(void)didSucceedRequest:(Patient *)patient;

@end

@interface DiagnoseService : NSObject

@property(nonatomic,assign)id<DiagnoseDelegate> delegate;
-(void)diagnosePatientWithName:(NSString *)name withAgeGroup:(NSString*)ageGroup withGender:
                                                          (NSString *)gender withMedicine:(NSString *)medicineUsage
                                                            withSymptoms:(NSString *)symptoms;
-(NSString *)getDateString;
@end
