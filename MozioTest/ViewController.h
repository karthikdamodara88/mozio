//
//  ViewController.h
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DiagnoseService.h"
#import "Patient.h"
#import "IQDropDownTextField.h"

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *patientNameField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *ageGroupField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *genderField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *medicineField;
@property (weak, nonatomic) IBOutlet IQDropDownTextField *symptomsField;

@end

