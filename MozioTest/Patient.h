//
//  Patient.h
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface Patient : NSManagedObject
@property(nonatomic, strong)NSString *patientName;
@property(nonatomic, strong)NSString *ageGroup;
@property(nonatomic, strong)NSString *gender;
@property(nonatomic, strong)NSString *symptoms;
@property(nonatomic,assign)NSString* medicineUsage;
@property(nonatomic, strong)NSString *date;
@property(nonatomic,strong)NSString *result;

@end
