//
//  Patient.m
//  MozioTest
//
//  Created by Karthik Damodara on 6/4/16.
//  Copyright © 2016 Karthik Damodara. All rights reserved.
//

#import "Patient.h"

@implementation Patient
@dynamic patientName;
@dynamic ageGroup;
@dynamic result;
@dynamic gender;
@dynamic medicineUsage;
@dynamic symptoms;
@dynamic date;

@end
